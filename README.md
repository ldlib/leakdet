# LeakDet

The LeakDet is a head file only, C++ memory leak detector.

# Quick Start

1. #include <leakdet.h> in your source file
2. put INIT_LD_LEAKDET()  in one of cpp file in your project.
3. call ld::initLeakDet() and ld::verifyLeakDet() methods in your source code

a simple example my_test.cpp

```cpp
#include"../leakdet.hpp"
INIT_LD_LEAKDET()

TEST(leakdet, no_mem_alloc)
{
    ld::initLeakDet();
    EXPECT_TRUE(ld::verifyLeakDet());
}
```

work with gtest. print like this.

```bash
Running main() from ./googletest/src/gtest_main.cc
[==========] Running 4 tests from 2 test suites.
[----------] Global test environment set-up.
[----------] 2 tests from leakdet
[ RUN      ] leakdet.no_mem_alloc
             no detect memory leaking.
[       OK ] leakdet.no_mem_alloc (0 ms)
[ RUN      ] leakdet.no_mem_leak
             no detect memory leaking.
[       OK ] leakdet.no_mem_leak (0 ms)
[----------] 2 tests from leakdet (0 ms total)[----------] 2 tests from good_code
[ RUN      ] good_code.det_mem_leak
[LEAKDET!!!] file: /home/lvd/workspace/ldlib/leakdet/test/test_leakdet.cpp, line: 25, memaddr: 0x560c20688820
[       OK ] good_code.det_mem_leak (0 ms)
[ RUN      ] good_code.release_and_destructor
             no detect memory leaking.
[       OK ] good_code.release_and_destructor (0 ms)
[----------] 2 tests from good_code (0 ms total)[----------] Global test environment tear-down
[==========] 4 tests from 2 test suites ran. (0 ms total)
[  PASSED  ] 4 tests.
```
