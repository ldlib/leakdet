cmake_minimum_required(VERSION 3.10)

project(test_leakdet VERSION 1.0.0 LANGUAGES CXX)

# set(CMAKE_CXX_STANDARD 11)
set(CMAKE_VERBOSE_MAKEFILE 1)

get_filename_component(MODULE_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/.." ABSOLUTE)
get_filename_component(LDLIB_ROOT "${MODULE_ROOT}/.." ABSOLUTE)
# get_filename_component(ThirdParty_ROOT "${LDLIB_ROOT}/../3rd-parties" ABSOLUTE)

message(STATUS "Project ROOT: ${CMAKE_CURRENT_SOURCE_DIR}")
message(STATUS "Module ROOT: ${MODULE_ROOT}")
message(STATUS "LDLIB ROOT: ${LDLIB_ROOT}")
# message(STATUS "3rd-parties ROOT: ${ThirdParty_ROOT}")

include_directories(
	${MODULE_ROOT}
	${LDLIB_ROOT}
	)


# Create the executable
FILE(GLOB_RECURSE SRCS 
	${MODULE_ROOT}/leakdet.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/test_leakdet.cpp
)

add_executable(${PROJECT_NAME} ${SRCS} )

target_link_libraries( ${PROJECT_NAME}
	gtest
    gtest_main
	pthread
	mockcpp
	)

enable_testing()

# add_test(${PROJECT_NAME} ${PROJECT_NAME}_test)

# string(TOLOWER ${CMAKE_BUILD_TYPE} _type)
# if(${_type} STREQUAL "debug")
#     include(CodeCoverage)
#     APPEND_COVERAGE_COMPILER_FLAGS()
#     SETUP_TARGET_FOR_COVERAGE_LCOV(NAME test_lcov 
#         EXECUTABLE ${PROJECT_NAME}
# 		EXCLUDE "/usr/*" "${MODULE_ROOT}/test/*" "${LDLIB_ROOT}/fsm/*"
#     )
# endif()


