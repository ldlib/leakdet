#include <gtest/gtest.h>
#include "../leakdet.hpp"
INIT_LD_LEAKDET()


TEST(leakdet, no_mem_alloc)
{
    ld::initLeakDet();
    EXPECT_TRUE(ld::verifyLeakDet());
}

TEST(leakdet, no_mem_leak)
{
    ld::initLeakDet();
    int *p = new int;
    char *q = new char[5];
    delete p;
    delete[] q;
    EXPECT_TRUE(ld::verifyLeakDet());
}

TEST(good_code, det_mem_leak)
{
    ld::initLeakDet();
    int *p = new int;
    char *q = new char[5];
    delete[] q;
    EXPECT_FALSE(ld::verifyLeakDet());
}

TEST(good_code, release_and_destructor)
{
    struct Test{
        int* p;
        Test():p(new int(9)){ }
        ~Test(){delete p;}
    };
    ld::initLeakDet();
    auto test = new Test[10];
    delete[] test;
    EXPECT_TRUE(ld::verifyLeakDet());
}
