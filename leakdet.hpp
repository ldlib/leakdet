/*
 * Copyright (c) 2006-2021, LD
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author       Notes
 * 2022-09-5     lvdong       the first version
 */

#pragma once

#include <cstddef>
#include <iostream>


#define LD_LIST_FOR_EACH(node, list)\
    for(node=(list)->next;node!=(list);node=node->next)

namespace ld{

struct LeakDetListNode
{
    void *ptr;
    const char *file;
    unsigned int line;
    LeakDetListNode *next;
    LeakDetListNode *pre;


    LeakDetListNode():
        ptr(nullptr),
        file(nullptr),
        line(0),
        next(nullptr),
        pre(nullptr){}

    void init(void* val, const char* file, unsigned int line)
    {
        this->ptr = val;
        this->file = file;
        this->line = line;
        this->next = this;
        this->pre = this;
    }

    void init()
    {
        init(nullptr, nullptr, 0);
    }

    void insertFront(LeakDetListNode* node)
    {
        if (node)
        {
            auto old_pre = node->pre;
            this->pre = old_pre;
            this->next = node;
            node->pre = this;
            old_pre->next = this;
        }
    }

    void insertBack(LeakDetListNode* node)
    {
        if (node)
        {
            auto old_next = node->next;
            this->pre = node;
            this->next = old_next;
            old_next->pre = this;
            node->next = this;
        }
    }

    void remove()
    {
        auto old_pre = this->pre;
        auto old_next = this->next;
        this->pre = this;
        this->next = this;
        old_pre->next = old_next;
        old_next->pre = old_pre;
    }

    bool isInList()
    {
        return (this->next != this) && (this->pre != this);
    }

};


class LeakDet
{
private:
    LeakDetListNode head;


    LeakDetListNode * find(const void*ptr)
    {
        auto node = head.next;
        while((node!=&head) && (node->ptr!=ptr))
        {
            node=node->next;
        }
        return (node == &head) ? nullptr : node;
    }


public:
    static LeakDet leak_detector;

    LeakDet() : 
        head()
    {
        head.init(nullptr,nullptr,0);
    }
    ~LeakDet()
    {
        this->init();
    }

    void insertMem(void *ptr, const char *file, unsigned int line)
    {
        LeakDetListNode *node = (LeakDetListNode *)malloc(sizeof(*node));
        node->init(ptr, file, line);
        node->insertFront(&this->head);
    }

    void deleteMem(void *ptr)
    {
        auto node = this->find(ptr);

        if (node != nullptr)
        {
            node->remove();
            free(node);
        }
    }

    void init()
    {
        auto node = head.next;
        while(node!= &head)
        {
            auto next = node->next;
            node->remove();
            free(node);
            node=next;
        }
    }

    bool verify()
    {
        bool result = true;
        if (this->head.isInList())
        {
            LeakDetListNode *node;
            LD_LIST_FOR_EACH(node, &head)
            {
                std::cout <<"\033[31m[LEAKDET!!!]\033[0m "
                          << "file: " << node->file
                          << ", line: " << node->line
                          << ", memaddr: " << node->ptr << std::endl;
            }

            result = false;
        }
        else
        {
            std::cout << "             no detect memory leaking." << std::endl;
        }

        return result;
    }
};


inline void initLeakDet()
{
  LeakDet::leak_detector.init();
}

/**
 * @brief verify no memory leak exists.
 * 
 * @return true  no memory leak exists.
 * @return false  detect memory leak.
 */
inline bool verifyLeakDet()
{
  return LeakDet::leak_detector.verify();
}

} /*namespace ld*/

inline void* operator new(size_t size, const char* file, unsigned int line) {
  void* ptr = malloc(size);
  ld::LeakDet::leak_detector.insertMem(ptr, file, line);
  return ptr;
}

inline void* operator new[](size_t size, const char* file, unsigned int line) {
  return operator new(size, file, line); 
}

inline void operator delete(void* ptr) {
  free(ptr);
  ld::LeakDet::leak_detector.deleteMem(ptr);
}

inline void operator delete[](void* ptr) {
  operator delete(ptr);
}

#define new new (__FILE__, __LINE__)

#define INIT_LD_LEAKDET() \
ld::LeakDet ld::LeakDet::leak_detector;
